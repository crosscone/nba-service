FROM nginx

COPY /public /public

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/conf.d/app.conf /etc/nginx/conf.d/app.conf

RUN nginx -t