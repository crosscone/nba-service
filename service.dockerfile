FROM golang:1.15.3

WORKDIR /go/src/app

# Install project dependencies

COPY go.mod go.mod
COPY go.sum go.sum

RUN go mod download

# Copy sources

COPY cmd/ cmd/
COPY pkg/ pkg/

# Build and install service

RUN go install \
    /go/src/app/cmd/service
