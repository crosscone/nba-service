package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

// Service handles incoming requests
type Service interface {
	http.Handler
}

// NewService creates new instance of Service using ServiceConfig
func NewService(logger log.Logger, container Container) (http.Handler, error) {
	router := mux.NewRouter()

	service := &service{
		logger:    logger,
		container: container,
		handler:   router,
	}

	router.HandleFunc("/v1/simulate", service.simulate).Methods("POST")
	router.HandleFunc("/v1/watch", service.watch)
	router.HandleFunc("/v1/health", service.health).Methods("GET")

	return service, nil
}

type service struct {
	logger    log.Logger
	container Container
	handler   http.Handler
}

type simulateRequest struct {
	MatchCount int `json:"match_count"`
}

func (s *service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.logRequest(r)
	s.handler.ServeHTTP(w, r)
}

// simulate allows to start match simulations
func (s *service) simulate(w http.ResponseWriter, r *http.Request) {
	var req simulateRequest
	body, _ := ioutil.ReadAll(r.Body)
	_ = json.Unmarshal(body, &req)

	ctx := r.Context()
	err := s.container.Simulation().Start(ctx, req.MatchCount)
	if err != nil {
		level.Error(s.logger).Log("err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// watch allows to watch match simulations changes
func (s *service) watch(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		level.Error(s.logger).Log("err", fmt.Sprintf("websocket upgrade: %s", err.Error()))
		return
	}

	defer c.Close()

	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			level.Error(s.logger).Log("err", fmt.Sprintf("websocket read: %s", err.Error()))
			break
		}

		err = c.WriteMessage(mt, message)
		if err != nil {
			level.Error(s.logger).Log("err", fmt.Sprintf("websocket write: %s", err.Error()))
			break
		}
	}
}

// health allows to perform service health check
func (s *service) health(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	err := s.container.Health().Check(ctx)
	if err != nil {
		errMsg := fmt.Sprintf("health check: %s", err.Error())
		level.Debug(s.logger).Log("err", errMsg)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Helper methods

func (s *service) logRequest(r *http.Request) {
	excludedPaths := map[string]bool{
		"/v1/health": true,
	}

	method := r.Method
	path := r.URL.Path
	if _, ok := excludedPaths[path]; ok {
		return
	}

	level.Debug(s.logger).Log("method", method, "path", path)
}

func (s *service) marshal(v interface{}) []byte {
	body, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		level.Error(s.logger).Log("err", err.Error())
	}

	return body
}
