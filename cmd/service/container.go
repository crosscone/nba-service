package main

import (
	"gitlab.com/crosscone/open/nba-service/pkg/game"
	"gitlab.com/crosscone/open/nba-service/pkg/health"
	"gitlab.com/crosscone/open/nba-service/pkg/simulation"

	"context"

	"github.com/go-kit/kit/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// Container represents NBA service container
// it contains various services required by NBA service
type Container interface {
	Game() game.Service
	Simulation() simulation.Service
	Health() health.Service
}

// NewContainer creates new NBA service container using ServiceConfig
func NewContainer(logger log.Logger, config ServiceConfig) (Container, error) {
	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(config.MongoURI()))
	if err != nil {
		return nil, err
	}

	err = mongoClient.Connect(context.Background())
	if err != nil {
		return nil, err
	}

	mongoClient.Ping(context.Background(), readpref.Primary())
	db := mongoClient.Database(config.MongoDB())

	gameRepository, err := game.NewRepository(db)
	if err != nil {
		return nil, err
	}

	gameService, err := game.NewService(logger, gameRepository)
	if err != nil {
		return nil, err
	}

	simulationService, err := simulation.NewService(logger, gameService)
	if err != nil {
		return nil, err
	}

	healthService, err := health.NewService(mongoClient)
	if err != nil {
		return nil, err
	}

	return &container{
		gameService:       gameService,
		simulationService: simulationService,
		healthService:     healthService,
	}, nil
}

type container struct {
	gameService       game.Service
	simulationService simulation.Service
	healthService     health.Service
}

func (c *container) Game() game.Service {
	return c.gameService
}

func (c *container) Simulation() simulation.Service {
	return c.simulationService
}

func (c *container) Health() health.Service {
	return c.healthService
}
