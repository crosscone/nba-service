package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/gorilla/handlers"
)

func main() {
	config, err := NewEnvServiceConfig()
	if err != nil {
		panic(err)
	}

	logger := log.NewLogfmtLogger(log.StdlibWriter{})
	logger = log.With(logger, "pod", config.PodName())

	level.Info(logger).Log("msg", "initializing service")

	container, err := NewContainer(logger, config)
	if err != nil {
		panic(err)
	}

	service, err := NewService(logger, container)
	if err != nil {
		panic(err)
	}

	level.Info(logger).Log("msg", "starting service")

	origins := handlers.AllowedOrigins([]string{"*"})
	methods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	handlers.CORS()

	server := &http.Server{
		Addr:         ":80",
		Handler:      handlers.CORS(origins, methods)(service),
		ReadTimeout:  time.Second,
		WriteTimeout: time.Second,
		IdleTimeout:  time.Second,
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		level.Info(logger).Log("msg", fmt.Sprintf("got %v signal, stopping", sig))
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			panic(err)
		}
	}()

	level.Info(logger).Log("msg", "serving")

	err = server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		panic(err)
	}
}
