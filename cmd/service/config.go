package main

import (
	"fmt"
	"os"
)

// ServiceConfig contains various configuration variables requires for NBA service set up
type ServiceConfig interface {
	PodName() string
	MongoURI() string
	MongoDB() string
}

// NewEnvServiceConfig allows to create service config from OS environment variables
func NewEnvServiceConfig() (ServiceConfig, error) {
	podName, err := getEnv("POD_NAME")
	if err != nil {
		return nil, err
	}

	mongoURI, err := getEnv("MONGO_URI")
	if err != nil {
		return nil, err
	}

	mongoDB, err := getEnv("MONGO_DB")
	if err != nil {
		return nil, err
	}

	return &serviceConfig{
		podName:  podName,
		mongoURI: mongoURI,
		mongoDB:  mongoDB,
	}, nil
}

type serviceConfig struct {
	podName  string
	mongoURI string
	mongoDB  string
}

func (s *serviceConfig) PodName() string {
	return s.podName
}

func (s *serviceConfig) MongoURI() string {
	return s.mongoURI
}

func (s *serviceConfig) MongoDB() string {
	return s.mongoDB
}

func getEnv(key string) (string, error) {
	res, ok := os.LookupEnv(key)
	if !ok {
		return "", fmt.Errorf("unable to get environment variable %s", key)
	}

	return res, nil
}
