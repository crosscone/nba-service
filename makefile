SERVICE_NAME = nba-service

DOCKER_ENV = eval $$(minikube docker-env);
IMAGE_DIGEST = docker inspect --format='{{index .Id}}' $(SERVICE_NAME)
IMAGE_DIGEST_STATIC = docker inspect --format='{{index .Id}}' $(SERVICE_NAME)-static

.PHONY: help
help: # Display this help
	@awk 'BEGIN{FS=":.*#";printf "Usage:\n  make <target>\n\nTargets:\n"}/^[a-zA-Z_-]+:.*?#/{printf"  %-10s %s\n",$$1,$$2}'$(MAKEFILE_LIST)

.PHONY: build
build: # Build service docker image
	$(DOCKER_ENV) docker build -f service.dockerfile -t $(SERVICE_NAME) .
	$(DOCKER_ENV) docker build -f static.dockerfile -t $(SERVICE_NAME)-static .

.PHONY: deploy
deploy: build # Deploy service (also builds docker image)
	$(DOCKER_ENV) helm upgrade --install --wait $(SERVICE_NAME) ./chart \
		--set image=$$($(IMAGE_DIGEST)) \
		--set imageStatic=$$($(IMAGE_DIGEST_STATIC))
		--set imagePullPolicy=Never
	kubectl rollout status deployment/$(SERVICE_NAME)
	@echo
	@echo '❕ Run "make http-static" to forward http port of static content'
	@echo
	@echo '❕ Run "make http-service" to forward http port of api service'
	@echo

.PHONY: delete
delete: # Delete service
	helm delete $(SERVICE_NAME)

.PHONY: attach
attach: # Attach to service shell
	kubectl exec -it deployment/$(SERVICE_NAME) sh

.PHONY: logs
logs: # Show service logs
	kubectl logs -f --tail=100 --all-containers=true -l app=$(SERVICE_NAME)

.PHONY: http-static
http-static: # Forward http port for static content
	@echo
	@echo '❕ You can access static content at http://127.0.0.1:8080'
	@echo
	kubectl port-forward --address 0.0.0.0 service/$(SERVICE_NAME)-static 8080:80

.PHONY: http-service
http-service: # Forward http port for api service
	@echo
	@echo '❕ You can access api service at http://127.0.0.1:8081'
	@echo
	kubectl port-forward --address 0.0.0.0 service/$(SERVICE_NAME) 8081:80
