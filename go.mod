module gitlab.com/crosscone/open/nba-service

go 1.15

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/go-kit/kit v0.10.0
	github.com/google/uuid v1.1.2
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	go.mongodb.org/mongo-driver v1.4.2
)
