package health

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// Service represents health service
type Service interface {
	Check(ctx context.Context) error
}

// NewService creates new health service
func NewService(client *mongo.Client) (Service, error) {
	return &service{
		client: client,
	}, nil
}

type service struct {
	client *mongo.Client
}

func (s *service) Check(ctx context.Context) error {
	return s.client.Ping(context.Background(), readpref.Primary())
}
