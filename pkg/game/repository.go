package game

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Repository represents game data repository
type Repository interface {
	CreateAction(context.Context, *Action) error
	GetMatchActions(context.Context, string) ([]*Action, error)

	CreateMatch(context.Context, *Match) error
	FinishMatch(context.Context, string, int64, int64) error
	GetActiveMatches(context.Context) ([]*Match, error)
	GetFinishedMatches(context.Context) ([]*Match, error)

	CreateTeam(context.Context, *Team) error
	GetAllTeams(ctx context.Context) ([]*Team, error)

	CreatePlayer(context.Context, *Player) error
	GetAllPlayers(ctx context.Context) ([]*Player, error)
}

// NewRepository creates new game data repository
func NewRepository(db *mongo.Database) (Repository, error) {
	return &repository{
		actionCollection: db.Collection(actionCollectionName),
		matchCollection:  db.Collection(matchCollectionName),
		teamCollection:   db.Collection(teamCollectionName),
		playerCollection: db.Collection(playerCollectionName),
	}, nil
}

const (
	actionCollectionName = "action"
	matchCollectionName  = "match"
	teamCollectionName   = "team"
	playerCollectionName = "player"
)

type repository struct {
	actionCollection *mongo.Collection
	matchCollection  *mongo.Collection
	teamCollection   *mongo.Collection
	playerCollection *mongo.Collection
}

func (r *repository) CreateAction(ctx context.Context, action *Action) error {
	_, err := r.actionCollection.InsertOne(ctx, action)
	return err
}

func (r *repository) GetMatchActions(ctx context.Context, matchID string) (res []*Action, err error) {
	filter := bson.M{"match_id": matchID}
	cur, err := r.actionCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	return res, cur.All(ctx, &res)
}

func (r *repository) CreateMatch(ctx context.Context, match *Match) error {
	_, err := r.matchCollection.InsertOne(ctx, match)
	return err
}

func (r *repository) FinishMatch(ctx context.Context, id string, teamAScore, teamBScore int64) error {
	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{"team_a_score": teamAScore, "team_b_score": teamBScore, "is_finished": true}}
	_, err := r.matchCollection.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *repository) GetActiveMatches(ctx context.Context) (res []*Match, err error) {
	filter := bson.M{"is_finished": bson.M{"$ne": true}}
	cur, err := r.matchCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	return res, cur.All(ctx, &res)
}

func (r *repository) GetFinishedMatches(ctx context.Context) (res []*Match, err error) {
	filter := bson.M{"is_finished": true}
	cur, err := r.matchCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	return res, cur.All(ctx, &res)
}

func (r *repository) CreateTeam(ctx context.Context, team *Team) error {
	_, err := r.teamCollection.InsertOne(ctx, team)
	return err
}

func (r *repository) GetAllTeams(ctx context.Context) (res []*Team, err error) {
	cur, err := r.teamCollection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	return res, cur.All(ctx, &res)
}

func (r *repository) CreatePlayer(ctx context.Context, player *Player) error {
	_, err := r.playerCollection.InsertOne(ctx, player)
	return err
}

func (r *repository) GetAllPlayers(ctx context.Context) (res []*Player, err error) {
	cur, err := r.playerCollection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	return res, cur.All(ctx, &res)
}
