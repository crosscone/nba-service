package game

import (
	"context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

// Service represents game service
type Service interface {
	CreateAction(context.Context, *Action) error
	GetMatchActions(context.Context, string) ([]*Action, error)

	CreateMatch(context.Context, *Match) error
	FinishMatch(context.Context, string, int64, int64) error
	GetActiveMatches(context.Context) ([]*Match, error)
	GetFinishedMatches(context.Context) ([]*Match, error)

	CreatePlayer(context.Context, *Player) error
	GetAllPlayers(context.Context) ([]*Player, error)

	CreateTeam(context.Context, *Team) error
	GetAllTeams(ctx context.Context) ([]*Team, error)
}

// NewService creates new game service
func NewService(logger log.Logger, repository Repository) (Service, error) {
	logger = log.With(logger, "service", "game")

	return &service{
		logger:     logger,
		repository: repository,
	}, nil
}

type service struct {
	logger     log.Logger
	repository Repository
}

func (s *service) CreateAction(ctx context.Context, action *Action) error {
	err := s.repository.CreateAction(ctx, action)
	if err != nil {
		level.Error(s.logger).Log("action", "CreateAction", "err", err.Error())
		return err
	}

	return nil
}

func (s *service) GetMatchActions(ctx context.Context, matchID string) ([]*Action, error) {
	actions, err := s.repository.GetMatchActions(ctx, matchID)
	if err != nil {
		level.Error(s.logger).Log("action", "GetMatchActions", "err", err.Error())
		return nil, err
	}

	return actions, nil
}

func (s *service) CreateMatch(ctx context.Context, match *Match) error {
	err := s.repository.CreateMatch(ctx, match)
	if err != nil {
		level.Error(s.logger).Log("action", "CreateMatch", "err", err.Error())
		return err
	}

	return nil
}

func (s *service) FinishMatch(ctx context.Context, id string, teamAScore, teamBScore int64) error {
	err := s.repository.FinishMatch(ctx, id, teamAScore, teamBScore)
	if err != nil {
		level.Error(s.logger).Log("action", "FinishMatch", "err", err.Error())
		return err
	}

	return nil
}

func (s *service) GetActiveMatches(ctx context.Context) ([]*Match, error) {
	matches, err := s.repository.GetActiveMatches(ctx)
	if err != nil {
		level.Error(s.logger).Log("action", "GetActiveMatches", "err", err.Error())
		return nil, err
	}

	return matches, nil
}

func (s *service) GetFinishedMatches(ctx context.Context) ([]*Match, error) {
	matches, err := s.repository.GetFinishedMatches(ctx)
	if err != nil {
		level.Error(s.logger).Log("action", "GetFinishedMatches", "err", err.Error())
		return nil, err
	}

	return matches, nil
}

func (s *service) CreateTeam(ctx context.Context, team *Team) error {
	err := s.repository.CreateTeam(ctx, team)
	if err != nil {
		level.Error(s.logger).Log("action", "CreateTeam", "err", err.Error())
		return err
	}

	return nil
}

func (s *service) GetAllTeams(ctx context.Context) ([]*Team, error) {
	teams, err := s.repository.GetAllTeams(ctx)
	if err != nil {
		level.Error(s.logger).Log("action", "GetAllTeams", "err", err.Error())
		return nil, err
	}

	return teams, nil
}

func (s *service) CreatePlayer(ctx context.Context, player *Player) error {
	err := s.repository.CreatePlayer(ctx, player)
	if err != nil {
		level.Error(s.logger).Log("action", "CreatePlayer", "err", err.Error())
		return err
	}

	return nil
}

func (s *service) GetAllPlayers(ctx context.Context) ([]*Player, error) {
	players, err := s.repository.GetAllPlayers(ctx)
	if err != nil {
		level.Error(s.logger).Log("action", "GetAllPlayers", "err", err.Error())
		return nil, err
	}

	return players, nil
}
