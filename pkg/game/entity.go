package game

// ActionKind represents action kind
type ActionKind int

// Available action kinds
const (
	ActionKindAttack ActionKind = iota
	ActionKindAssist
)

// Action represents action entity
type Action struct {
	ID       string     `json:"id" bson:"_id"`
	MatchID  string     `json:"match_id" bson:"match_id"`
	PlayerID string     `json:"player_id" bson:"player_id"`
	Score    int64      `json:"score" bson:"score"`
	Kind     ActionKind `json:"action_kind" bson:"action_kind"`
}

// Match represents match entity
type Match struct {
	ID         string `json:"id" bson:"_id"`
	TeamAID    string `json:"team_a_id" bson:"team_a_id"`
	TeamBID    string `json:"team_b_id" bson:"team_b_id"`
	TeamAScore int64  `json:"team_a_score" bson:"team_a_score"`
	TeamBScore int64  `json:"team_b_score" bson:"team_b_score"`
	IsFinished bool   `json:"is_finished" bson:"is_finished"`
}

// Team represents team entity
type Team struct {
	ID   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
}

// Player represents player entity
type Player struct {
	ID     string `json:"id" bson:"_id"`
	TeamID string `json:"team_id" bson:"team_id"`
	Name   string `json:"name" bson:"name"`
}
