package simulation

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/Pallinder/go-randomdata"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/google/uuid"
	"gitlab.com/crosscone/open/nba-service/pkg/game"
)

// Service represents simulation service
type Service interface {
	Start(context.Context, int) error
}

// NewService creates new simulation service
func NewService(logger log.Logger, gameService game.Service) (Service, error) {
	logger = log.With(logger, "service", "simulation")

	return &service{
		logger:      logger,
		gameService: gameService,
		rand:        rand.New(rand.NewSource(time.Now().Unix())),
	}, nil
}

const (
	playersInTeam int = 15
)

type service struct {
	logger      log.Logger
	gameService game.Service
	rand        *rand.Rand
}

func (s *service) Start(ctx context.Context, matchCount int) error {
	level.Debug(s.logger).Log("msg", fmt.Sprintf("starting simulation for %d maches", matchCount))

	for i := 0; i < matchCount; i++ {
		sim, err := s.createMatchSimulation(ctx)
		if err != nil {
			errMsg := fmt.Sprintf("create simulation: %s", err.Error())
			level.Error(s.logger).Log("err", errMsg)
			return err
		}

		go func(sim *simulation) {
			err = s.runSimulation(context.Background(), sim)
			if err != nil {
				errMsg := fmt.Sprintf("run simulation: %s", err.Error())
				level.Error(s.logger).Log("err", errMsg)
			}
		}(sim)

	}

	return nil
}

type simulation struct {
	match        *game.Match
	teamA        *game.Team
	teamB        *game.Team
	teamAPlayers []*game.Player
	teamBPlayers []*game.Player
}

func (s *service) createMatchSimulation(ctx context.Context) (*simulation, error) {
	teamA, err := s.createTeam(ctx)
	if err != nil {
		return nil, err
	}

	teamB, err := s.createTeam(ctx)
	if err != nil {
		return nil, err
	}

	teamAPlayers := make([]*game.Player, playersInTeam)
	for i := 0; i < playersInTeam; i++ {
		player, err := s.createPlayer(ctx, teamA.ID)
		if err != nil {
			return nil, err
		}

		teamAPlayers[i] = player
	}

	teamBPlayers := make([]*game.Player, playersInTeam)
	for i := 0; i < playersInTeam; i++ {
		player, err := s.createPlayer(ctx, teamB.ID)
		if err != nil {
			return nil, err
		}

		teamBPlayers[i] = player
	}

	match, err := s.createMatch(ctx, teamA.ID, teamB.ID)
	if err != nil {
		return nil, err
	}

	return &simulation{
		match:        match,
		teamA:        teamA,
		teamB:        teamB,
		teamAPlayers: teamAPlayers,
		teamBPlayers: teamBPlayers,
	}, nil
}

func (s *service) runSimulation(ctx context.Context, sim *simulation) error {
	logger := log.With(s.logger, "team_a", sim.teamA.Name, "team_b", sim.teamB.Name)
	level.Debug(logger).Log("msg", "starting simulation")

	deadline := time.After(time.Second * 240)

	for {
		minAttackTime := int64(time.Second * 12)
		maxAttackTime := int64(time.Second * 24)
		nextAtackTime := s.randInt64(minAttackTime, maxAttackTime)

		select {
		case <-deadline:
			level.Debug(logger).Log("msg", "finishing simulation")
			s.gameService.FinishMatch(ctx, sim.match.ID, 0, 0)
			return nil
		case <-time.After(time.Duration(nextAtackTime / 12)):
			action, err := s.createAction(ctx, sim)
			if err != nil {
				return err
			}

			level.Debug(s.logger).Log(
				"match", sim.match.ID,
				"player", action.PlayerID,
				"msg", "new action",
			)
			break
		}
	}
}

func (s *service) createMatch(ctx context.Context, teamAID, teamBID string) (*game.Match, error) {
	match := &game.Match{
		ID:      uuid.New().String(),
		TeamAID: teamAID,
		TeamBID: teamBID,
	}

	return match, s.gameService.CreateMatch(ctx, match)
}

func (s *service) createAction(ctx context.Context, sim *simulation) (*game.Action, error) {
	players := []*game.Player{}
	teamNumber := s.randInt64(0, 1)

	switch teamNumber {
	case 0:
		players = sim.teamAPlayers
		break
	case 1:
		players = sim.teamBPlayers
		break
	}

	playerCount := len(players)
	action := &game.Action{
		ID:       uuid.New().String(),
		MatchID:  sim.match.ID,
		PlayerID: players[s.randInt64(0, int64(playerCount)-1)].ID,
		Score:    s.randInt64(0, 3),
	}

	return action, s.gameService.CreateAction(ctx, action)
}

func (s *service) createTeam(ctx context.Context) (*game.Team, error) {
	team := &game.Team{
		ID:   uuid.New().String(),
		Name: randomdata.SillyName(),
	}

	return team, s.gameService.CreateTeam(ctx, team)
}

func (s *service) createPlayer(ctx context.Context, teamID string) (*game.Player, error) {
	player := &game.Player{
		ID:     uuid.New().String(),
		TeamID: teamID,
		Name:   randomdata.FullName(randomdata.Male),
	}

	return player, s.gameService.CreatePlayer(ctx, player)
}

func (s *service) randInt64(min, max int64) int64 {
	return s.rand.Int63n(max-min+1) + min
}
