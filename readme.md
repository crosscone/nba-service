# NBA Service

This service allows to run NBA match simulations and watch statistics in realtime

- service uses GitLab pipelines for build and deployment
- service uses MongoDB as persistent data store
- service uses WebSocket for realtime communication between client (web browser) and server (golang service)
- service is deployed to kubernetes cluster
- service uses helm to manage service releases inside kubernetes cluster
- service has makefile with useful commands in the root directory to simplify local development and testing

## Demo Site

Demo site is available at [https://nba.crosscone.com/](https://nba.crosscone.com/)

## Development Requirements

For successful deployment, you need these tools to be installed on your system:

- [docker](https://docs.docker.com/get-docker/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [helm](https://helm.sh/docs/intro/install/)
- make

## Usage

`$ make help`

```
Usage:
  make <target>

Targets:
  help        Display this help
  build       Build service docker image
  deploy      Deploy service (also builds docker image)
  delete      Delete service
  attach      Attach to service shell
  logs        Show service logs
  http        Forward http port
```